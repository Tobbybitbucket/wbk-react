import React from "react";
import ReactDOM from "react-dom";
import "./css/main.scss";


let Body = () => {
    console.log("ENTRY POINT: inside index.js");
    return <h1>this is the React part</h1>
}
ReactDOM.render(
    <Body />,
    document.getElementById("root")
);


